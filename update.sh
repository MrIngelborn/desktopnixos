#!/usr/bin/env bash

echo "The script you are running has:"
echo "basename: [$(basename "$0")]"
echo "dirname : [$(dirname "$0")]"
echo "pwd     : [$(pwd)]"

cd $(dirname $0)

nix flake update --commit-lock-file

nixos-rebuild build --flake . 

nvd diff /run/current-system result

git push

sudo nixos-rebuild switch --flake .
