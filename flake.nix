{
  outputs = {
    self,
    nixpkgs,
    ...
  } @ inputs:
    (
      inputs.snowfall-lib.mkFlake {
        inherit inputs;
        src = ./.;
        systems.modules.nixos = with inputs; [
          {nix.settings.trusted-users = ["mringelborn"];}
          nixvim.nixosModules.nixvim
          stylix.nixosModules.stylix
          catppuccin.nixosModules.catppuccin
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
          }
        ];
        homes.modules = with inputs; [
          catppuccin.homeManagerModules.catppuccin
        ];
        outputs-builder = channels: {
          formatter = channels.nixpkgs.alejandra;
        };
        snowfall.namespace = "mringelborn";
        channels-config.allowUnfree = true;
      }
    )
    // {
      devShells.x86_64-linux.default = inputs.nixpkgs.legacyPackages.x86_64-linux.mkShell {
        inherit (self.checks.x86_64-linux.pre-commit-check) shellHook;
        buildInputs = self.checks.x86_64-linux.pre-commit-check.enabledPackages;
      };
    };
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    snowfall-lib = {
      url = "github:snowfallorg/lib";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    snowflakeos-modules.url = "github:snowflakelinux/snowflakeos-modules";
    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nur = {
      url = "github:nix-community/NUR";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    hyprland-plugins = {
      url = "github:hyprwm/hyprland-plugins";
    };
    nixvim = {
      url = "github:nix-community/nixvim";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    stylix.url = "github:danth/stylix/release-24.11";
    base16-wallpapers = {
      url = "github:MrIngelborn/base16-wallpapers";
      flake = false;
    };
    lodobo_base16-wallpapers = {
      url = "github:Lodobo/base16-wallpapers";
      flake = false;
    };
    base16-jetbrains = {
      url = "github:tinted-theming/base16-jetbrains";
      flake = false;
    };
    catppuccin.url = "github:catppuccin/nix";
    pre-commit-hooks.url = "github:cachix/git-hooks.nix";
  };
  nixConfig = {
    extra-substituters = [
      "http://192.168.1.37"
      "https://nix-community.cachix.org"
      "https://hyprland.cachix.org"
      "https://snowflakeos.cachix.org"
    ];
    extra-trusted-public-keys = [
      "192.168.1.37:Obb4lWL8YLwhJmFcqbyT1ERdVZwI2UJuSxzlN/dGN9U="
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      "hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="
      "snowflakeos.cachix.org-1:gXb32BL86r9bw1kBiw9AJuIkqN49xBvPd1ZW8YlqO70="
    ];
  };
}
