{pkgs}: let
  pactl = "${pkgs.pulseaudio}/bin/pactl";
  jq = "${pkgs.jq}/bin/jq";
  signal = 3;
  script = pkgs.writeShellScript "waybar-pa-sink" ''
    function getCurrentSinkName {
      echo $(${pactl} get-default-sink)
    }
    sinkListJson="$(${pactl} -f json list sinks)"

    indexList="$(echo $sinkListJson | ${jq} '. as $list | keys | map({i: ., name: $list[.].name, description: $list[.].description})')"

    function getSinkLength {
      echo $indexList | ${jq} ". | length"
    }

    function getCurrentSinkIndex {
      echo $indexList | ${jq} ".[] | select(.name == \"$(getCurrentSinkName)\") | .i"
    }

    function getCurrentSinkDescription {
      echo $indexList | ${jq} ".[$(getCurrentSinkIndex)].description "
    }

    function indexToName {
      echo $indexList | ${jq} ".[$1].name"
    }

    function setIndexAndSignalUpdate {
      name=$(indexToName $1)
      ${pactl} set-default-sink ''${name:1:-1}
      pkill -RTMIN+${toString signal} waybar
    }


    case $1 in
      get)
        currentSinkDescription=$(getCurrentSinkDescription)
        echo ''${currentSinkDescription:1:-1}
        ;;
      next)
        currentSinkIndex=$(getCurrentSinkIndex)
        nextIndex="$((($currentSinkIndex+1)%$(getSinkLength)))"
        setIndexAndSignalUpdate $nextIndex
        ;;
      prev)
        currentSinkIndex=$(getCurrentSinkIndex)
        prevIndex="$((($currentSinkIndex-1)%$(getSinkLength)))"
        setIndexAndSignalUpdate $prevIndex
        ;;
    esac
  '';
in {
  exec = "${script} get";
  exec-on-event = false;
  interval = "once";
  on-scroll-up = "${script} prev";
  on-scroll-down = "${script} next";
  inherit signal;
  tooltip = false;
}
