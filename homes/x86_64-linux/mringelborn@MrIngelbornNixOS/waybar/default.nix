{
  pkgs,
  vars,
  lib,
  ...
}: {
  programs.waybar = {
    enable = true;
    settings = {
      topBar = {
        layer = "top";
        modules-left = [
          "tray"
        ];
        modules-center = [
          "hyprland/workspaces"
        ];
        modules-right = [
          "gamemode"
          "idle_inhibitor"
          "custom/pa-sink"
          "pulseaudio"

          "clock"
          "group/power"
        ];
        "idle_inhibitor" = {
          format = "{icon}";
          format-icons = {
            activated = "";
            deactivated = "";
          };
          tooltip = "true";
        };
        pulseaudio = {
          format = "{icon} {volume}% {format_source}";
          format-bluetooth = " {icon} {volume}% {format_source}";
          format-bluetooth-muted = "  {format_source}";
          format-muted = " {format_source}";
          format-source = " {volume}%";
          format-source-muted = "";
          format-icons = {
            headphone = "";
            hands-free = "";
            headset = "";
            phone = "";
            portable = "";
            car = "";
            default = ["" "" ""];
          };
          on-click = "${pkgs.pavucontrol}/bin/pavucontrol";
        };
        "custom/pa-sink" = import ./pa-sink.nix {inherit pkgs;};
        "group/power" = {
          orientation = "inherit";
          drawer = {
            transition-duration = 500;
            children-class = "not-power";
            transition-left-to-right = false;
          };
          modules = [
            "custom/power" # First element is the "group leader" and won't ever be hidden
            "custom/quit"
            "custom/lock"
            "custom/reboot"
          ];
        };
        "custom/quit" = {
          "format" = " ";
          "tooltip" = false;
          "on-click" = "hyprctl dispatch exit";
        };
        "custom/lock" = {
          "format" = " ";
          "tooltip" = false;
          "on-click" = vars.lockcommand;
        };
        "custom/reboot" = {
          "format" = " ";
          "tooltip" = false;
          "on-click" = "reboot";
        };
        "custom/power" = {
          "format" = "";
          "tooltip" = false;
          "on-click" = "shutdown now";
        };
        clock = {
          format = " {:%H:%M}";
          tooltip = true;
          tooltip-format = "<big>{:%A, %d %B %Y }</big>";
        };
      };
    };
    style = let
      indices = ["1" "2" "3" "4" "5" "6" "7" "8"];
      hexes = ["8" "9" "A" "B" "C" "D" "E" "F"];
    in
      ''
        widget > * {
          border-radius: 5px;
          padding: 0 5px;
          margin: 0;
          color: @base00;
        }
      ''
      + (lib.strings.concatLines (
        lib.lists.zipListsWith (i: hex: ''
          .modules-right > *:nth-last-child(${i}) > * {
            background: @base0${hex};
          }
        '')
        indices
        hexes
      ))
      + (lib.strings.concatLines (
        lib.lists.zipListsWith (i: hex: ''
          #tray > widget.active:nth-child(8n+${i}) > * {
            background-color: @base0${hex};
          }
        '')
        indices
        hexes
      ));
  };
}
