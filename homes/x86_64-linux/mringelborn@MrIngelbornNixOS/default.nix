{
  pkgs,
  home,
  inputs,
  ...
}: {
  home = {
    stateVersion = "23.11";
    username = "mringelborn";
    homeDirectory = "/home/mringelborn";
  };

  imports = [
    inputs.nixvim.homeManagerModules.nixvim
  ];

  mringelborn = {
    firefox.enable = true;
    base16.enable = true;
  };

  programs = {
    home-manager.enable = true;
    alacritty.enable = true;
    fish.enable = true;
    nixvim = {
      enable = true;
      plugins = {
        nix.enable = true;
      };
      opts = {
        number = true;
        expandtab = true;
        shiftwidth = 2;
        tabstop = 2;
      };
    };
  };

  home.packages = with pkgs; [
    font-awesome
    dconf
    adwaita-qt
    kdePackages.kasts
    bitwarden
    thunderbird
    protonmail-bridge
    mullvad-browser
    tor-browser
  ];

  dconf.enable = true;
  qt.enable = true;
  gtk.enable = true;

  services = {
    dunst = {
      enable = false;
      settings = {
      };
    };

    protonmail-bridge.enable = true;
    celeste = {
      enable = true;
      remotes = {
        "Nextcloud" = [
          {
            localPath = "/home/mringelborn/Games/Heroic/Prefixes/default/Baldurs Gate 3/pfx/drive_c/users/steamuser/AppData/Local/Larian Studios/Baldur's Gate 3/PlayerProfiles/Public/Savegames/Story";
            remotePath = "Spel/Save files/BG3";
          }
          {
            localPath = "/home/mringelborn/Pictures/Screenshots";
            remotePath = "Bilder/Screenshots";
          }
        ];
        "ASDF" = [
          {
            localPath = "/asdf/qwerty/";
            remotePath = "remote/path";
          }
        ];
      };
    };

    kdeconnect = {
      enable = true;
      indicator = true;
    };
  };
}
