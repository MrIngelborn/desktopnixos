{pkgs, ...}: {
  programs.home-manager.enable = true;

  home = {
    username = "jennysimmar";
    homeDirectory = "/home/jennysimmar";

    packages = with pkgs; [
      darktable
    ];

    stateVersion = "23.11";
  };
}
