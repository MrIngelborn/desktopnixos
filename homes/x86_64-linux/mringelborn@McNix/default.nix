{
  home = {
    stateVersion = "23.11";

    username = "mringelborn";
    homeDirectory = "/home/mringelborn";
    shellAliases = {
      gl = "git log --pretty=format:'%C(auto)%h%C(blue) %ad%C(auto)%d %s' --date=format:'%Y-%m-%d %H:%M'";
    };
  };

  services.protonmail-bridge = {
    enable = true;
    logLevel = "debug";
  };

  programs = {
    home-manager.enable = true;
    git = {
      enable = true;
      userName = "Marcus Ingelborn";
      userEmail = "git@mringelborn.com";
    };
  };

  dconf.settings = {
    "org/gnome/mutter" = {
      experimental-features = ["scale-monitor-framebuffer"];
    };
  };
}
