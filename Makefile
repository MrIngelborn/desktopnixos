.PHONY: default build install update diff history clean

switch:
	nix-shell -p nh --run "nh os switch -a . -- --accept-flake-config"

build:
	nix-shell -p nix-output-monitor --run "nixos-rebuild build --flake . --accept-flake-config --log-format internal-json -v |& nom --json"

install:
	nixos-rebuild switch --flake . --accept-flake-config --use-remote-sudo 

update:
	nix flake update --commit-lock-file

push:
	git push

history:
	nix profile history --profile /nix/var/nix/profiles/system

home:
	nix-shell -p nh --run "nh home switch -a ."

clean: 
	nix-shell -p nh --run "nh clean all -k 3 -K 7d -a"
