# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  pkgs,
  inputs,
  ...
}: {
  imports = [
    inputs.nixos-hardware.nixosModules.common-cpu-intel
    inputs.nixos-hardware.nixosModules.common-gpu-amd
    inputs.nixos-hardware.nixosModules.common-pc-ssd

    ./hardware-configuration.nix
  ];

  mringelborn = {
    common.enable = true;
    games.enable = true;
    develop = {
      enable = true;
      intellij.plugins = [
        "nixidea"
      ];
    };
    base16 = {
      enable = true;
      scheme = "gigavolt";
    };
    catppuccin.enable = false;
  };

  boot.kernelModules = [
    "nct6775"
  ];

  networking.hostName = "MrIngelbornNixOS";

  services = {
    # Enable the X11 windowing system.
    xserver.enable = true;

    # Enable KDE Plasma 6
    displayManager.sddm.enable = true;
    displayManager.sddm.wayland.enable = true;
    desktopManager.plasma6.enable = true;

    gnome.gnome-keyring.enable = true;

    hardware.openrgb.enable = true;
    fwupd.enable = true;
  };

  users = {
    defaultUserShell = pkgs.fish;
    users = {
      mringelborn = {
        isNormalUser = true;
        description = "Marcus Ingelborn";
        extraGroups = ["networkmanager" "wheel"];
      };
      jennysimmar = {
        isNormalUser = true;
        description = "Jenny Simm";
        extraGroups = ["networkmanager"];
      };
    };
  };

  environment.systemPackages = with pkgs; [
    htop
    wget
    git
    neofetch
    celeste
    deja-dup
    libreoffice
    scribus
    mission-center
    signal-desktop
    spotify
    transmission_4-gtk
    vlc
    nvd
    alacritty
    lm_sensors
    gnumake
    home-manager
    nix-output-monitor
  ];

  programs = {
    kdeconnect.enable = true;
    fish.enable = true;
    nixvim.enable = true;
    neovim = {
      enable = true;
      vimAlias = true;
    };
    firefox.enable = true;
    coolercontrol.enable = true;
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?
}
