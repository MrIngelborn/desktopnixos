{
  modulesPath,
  pkgs,
  ...
}: {
  imports = [
    ./hardware.nix
    (modulesPath + "/virtualisation/proxmox-lxc.nix")
  ];

  networking.firewall.allowedTCPPorts = [443 80];

  services = {
    harmonia = {
      enable = true;
      signKeyPaths = ["/var/cache-priv-key.pem"];
    };
    nginx = {
      enable = true;
      package = pkgs.nginxStable.override {
        modules = [pkgs.nginxModules.zstd];
      };
      recommendedTlsSettings = true;
      recommendedZstdSettings = true;
      virtualHosts."192.168.1.37" = {
        locations."/".extraConfig = ''
          proxy_pass http://127.0.0.1:5000;
          proxy_set_header Host $host;
          proxy_redirect http:// https://;
          proxy_http_version 1.1;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection $connection_upgrade;

          zstd on;
          zstd_types application/x-nix-archive;
        '';
      };
    };
  };

  systemd.timers.nix-build = {
    wantedBy = ["timers.target"];
    timerConfig = {
      OnCalendar = "hourly";
      Persistent = true;
      Unit = "nix-build.service";
    };
  };
  systemd.services.nix-build = {
    serviceConfig = {
      Type = "oneshot";
      User = "root";
    };
    description = "Build Nix Config";
    after = ["network.target"];
    script = let
      buildConfig = name: "${pkgs.nixos-rebuild}/bin/nixos-rebuild build --refresh --flake gitlab:MrIngelborn/desktopnixos#${name} --accept-flake-config --no-write-lock-file";
    in ''
      ${buildConfig "MrIngelbornNixOS"}
      ${buildConfig "snowflakeos"}
    '';
  };

  system.stateVersion = "24.11";
}
