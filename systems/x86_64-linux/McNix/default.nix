{
  pkgs,
  inputs,
  ...
}: {
  imports = [
    ./hardware.nix
    inputs.snowflakeos-modules.nixosModules.efiboot
    inputs.snowflakeos-modules.nixosModules.gnome
    inputs.snowflakeos-modules.nixosModules.kernel
    inputs.snowflakeos-modules.nixosModules.networking
    inputs.snowflakeos-modules.nixosModules.pipewire
    inputs.snowflakeos-modules.nixosModules.snowflakeos
    inputs.snowflakeos-modules.nixosModules.metadata
    inputs.nixos-hardware.nixosModules.apple-macbook-pro
  ];

  mringelborn = {
    common.enable = true;
    develop = {
      enable = true;
      intellij.plugins = [
        "nixidea"
      ];
    };
  };
  modules.gnome.removeUtils.enable = true;

  hardware.facetimehd.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users."mringelborn" = {
    isNormalUser = true;
    description = "MrIngelborn";
    extraGroups = ["wheel" "networkmanager" "dialout"];
  };

  # List packages installed in system profile.
  environment.systemPackages = with pkgs; [
    vim
    thunderbird
    protonmail-bridge
    neofetch
    bitwarden
    gnome-text-editor
    gnomeExtensions.dash-to-dock
    gnomeExtensions.appindicator
    discord
    signal-desktop
    nvd
    gnumake
  ];

  programs = {
    firefox.enable = true;
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?
}
