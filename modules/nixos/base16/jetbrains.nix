{
  config,
  inputs,
  lib,
  pkgs,
  ...
}: let
  base16Cfg = config.mringelborn.base16;
  intellijCfg = config.mringelborn.develop.intellij;

  base16theme = let
    editorTheme = config.lib.stylix.colors {
      templateRepo = inputs.base16-jetbrains;
      target = "editor-theme";
    };
    ideTheme = config.lib.stylix.colors {
      templateRepo = inputs.base16-jetbrains;
      target = "ide-theme";
    };
    plugin = config.lib.stylix.colors {
      templateRepo = inputs.base16-jetbrains;
      target = "plugin";
    };
    slug = config.lib.stylix.colors.scheme-slug;
  in
    pkgs.stdenv.mkDerivation {
      name = "base16_theme.jar";
      nativeBuildInputs = [pkgs.zip];
      phases = ["buildPhase" "installPhase"];
      buildPhase = ''
        # https://github.com/tinted-theming/base16-jetbrains/blob/main/build.sh

        cp ${editorTheme} base16-${slug}.xml
        cp ${ideTheme} base16-${slug}.theme.json

        mkdir META-INF
        touch META-INF/MANIFEST.MF
        echo -e "Manifest-Version: 1.0\nCreated-By: IntelliJ IDEA" > META-INF/MANIFEST.MF

        cp ${plugin} META-INF/plugin.xml

        zip -r base16.jar $(find * -type f)
      '';
      installPhase = ''
        cp base16.jar $out
      '';
    };
in {
  config = lib.mkIf (base16Cfg.enable && intellijCfg.enable) {
    mringelborn.develop.intellij.plugins = [
      base16theme
    ];
  };
}
