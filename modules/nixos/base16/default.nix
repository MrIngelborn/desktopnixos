{
  config,
  inputs,
  lib,
  pkgs,
  ...
}: let
  cfg = config.mringelborn.base16;

  convertSvgToPng = name: svg: (pkgs.stdenv.mkDerivation {
    name = "${name}.png";
    nativeBuildInputs = [pkgs.imagemagick];
    phases = ["buildPhase" "installPhase"];
    buildPhase = ''
      convert -resize 3840x2160 -depth 8 ${svg} converted.png
    '';
    installPhase = ''
      cp converted.png $out
    '';
  });
  wallpaper-base16-nix = convertSvgToPng "wallpaper-base16-nix" (config.lib.stylix.colors {
    templateRepo = inputs.lodobo_base16-wallpapers;
    target = cfg.wallpaperTemplate;
  });
in
  with lib; {
    imports = [
      ./jetbrains.nix
    ];
    options.mringelborn.base16 = {
      enable = mkEnableOption "MrIngelborn Base16";
      scheme = mkOption {
        type = types.str;
      };
      wallpaperTemplate = mkOption {
        type = types.str;
        default = "ascii-cat_2";
      };
    };
    config = mkIf cfg.enable {
      stylix = {
        enable = true;
        image = wallpaper-base16-nix;
        base16Scheme = "${pkgs.base16-schemes}/share/themes/${cfg.scheme}.yaml";
      };
    };
  }
