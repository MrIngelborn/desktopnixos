{
  config,
  pkgs,
  lib,
  ...
}: {
  config = lib.mkIf config.mringelborn.common.enable {
    services.printing = {
      enable = true;
      drivers = with pkgs; [
        epson-escpr
      ];
    };
    services.avahi = {
      enable = true;
      nssmdns4 = true;
      openFirewall = true;
    };
    hardware.sane = {
      enable = true;
      extraBackends = with pkgs; [
        epsonscan2
      ];
    };
    nixpkgs.overlays = [
      (_final: prev: {
        epsonscan2 = prev.epsonscan2.override {
          withNonFreePlugins = true;
        };
      })
    ];
  };
}
