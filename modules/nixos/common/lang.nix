{
  config,
  lib,
  ...
}: {
  config = lib.mkIf config.mringelborn.common.enable {
    time.timeZone = "Europe/Stockholm";
    i18n.defaultLocale = "en_GB.UTF-8";
    i18n.extraLocaleSettings = {
      LC_ADDRESS = "sv_SE.UTF-8";
      LC_IDENTIFICATION = "sv_SE.UTF-8";
      LC_MEASUREMENT = "sv_SE.UTF-8";
      LC_MONETARY = "sv_SE.UTF-8";
      LC_NAME = "sv_SE.UTF-8";
      LC_NUMERIC = "sv_SE.UTF-8";
      LC_PAPER = "sv_SE.UTF-8";
      LC_TELEPHONE = "sv_SE.UTF-8";
      LC_TIME = "sv_SE.UTF-8";
    };
    services.xserver.xkb = {
      layout = "se";
      variant = "nodeadkeys";
    };

    console.useXkbConfig = true;
  };
}
