{
  config,
  lib,
  ...
}: {
  config = lib.mkIf config.mringelborn.common.enable {
    networking = {
      networkmanager.enable = true;
      firewall.enable = true;
    };
  };
}
