{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.mringelborn.common;
in
  with lib; {
    imports = [
      ./lang.nix
      ./printing.nix
      ./networking.nix
      ./sound.nix
    ];
    options.mringelborn.common = with lib.mringelborn; {
      enable = mkEnableOption "MrIngelborn Common";
    };
    config = mkIf cfg.enable {
      home-manager.backupFileExtension = "bkp";
      nix.settings.experimental-features = ["nix-command" "flakes"];

      environment.sessionVariables.NIXPKGS_ALLOW_UNFREE = "1";

      # Bootloader.
      boot = {
        loader.systemd-boot.enable = true;
        loader.efi.canTouchEfiVariables = true;

        kernelPackages = pkgs.linuxPackages_latest;
      };

      services.kmscon.enable = true;
    };
  }
