{
  config,
  lib,
  ...
}: let
  cfg = config.mringelborn.catppuccin;
in
  with lib; {
    imports = [
      ./kde.nix
      ./cursors.nix
    ];

    options.mringelborn.catppuccin = {
      enable = mkEnableOption "MrIngelborn Catppuccin";
    };

    config = mkIf cfg.enable {
      catppuccin = {
        enable = true;
        accent = "sky";
      };
    };
  }
