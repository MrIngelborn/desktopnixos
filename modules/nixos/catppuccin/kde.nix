{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.mringelborn.catppuccin;
  enable = cfg.enable && config.services.desktopManager.plasma6.enable;
  catppuccin-kde = pkgs.catppuccin-kde.override {
    flavour = [config.catppuccin.flavor];
    accents = [config.catppuccin.accent];
  };
in
  with lib; {
    config = mkIf enable {
      environment.systemPackages = [
        catppuccin-kde
      ];
    };
  }
