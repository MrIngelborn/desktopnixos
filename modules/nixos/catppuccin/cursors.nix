{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.mringelborn.catppuccin;

  inherit (config.catppuccin) flavor;
  inherit (lib.mringelborn.toUpperFirstChar config.catppuccin) accent;
  packageName = flavor + accent;
in
  with lib; {
    config = mkIf cfg.enable {
      environment.systemPackages = [
        pkgs.catppuccin-cursors.${packageName}
      ];
    };
  }
