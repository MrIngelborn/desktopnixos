{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.mringelborn.develop;
  intellij = with pkgs.jetbrains; plugins.addPlugins idea-ultimate cfg.intellij.plugins;
in
  with lib; {
    options.mringelborn.develop = {
      enable = mkEnableOption "MrIngelborn Development tools";
      intellij = {
        enable = mkEnableOption "Jetbrains IDEA Ultimate" // {default = cfg.enable;};
        plugins = mkOption {
          type = with types; listOf (oneOf [package str]);
          default = [];
        };
      };
    };
    config = mkIf cfg.enable {
      environment.systemPackages = with pkgs; [
        intellij
      ];
    };
  }
