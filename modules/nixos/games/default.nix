{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.mringelborn.games;
in
  with lib; {
    options.mringelborn.games = {
      enable = mkEnableOption "Game Options";
    };

    config = mkIf cfg.enable {
      programs = {
        steam = {
          enable = true;
          remotePlay.openFirewall = true;
          dedicatedServer.openFirewall = true;
          gamescopeSession.enable = true;
          extraCompatPackages = [
            pkgs.proton-ge-bin
          ];
        };
        gamemode.enable = true;
        gamescope.enable = true;
      };
      environment.systemPackages = with pkgs; [
        # Platforms
        heroic
        lutris
        bottles

        # Communications
        discord
        teamspeak_client
      ];
    };
  }
