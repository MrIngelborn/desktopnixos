{
  lib,
  config,
  ...
}: let
  cfg = config.mringelborn.hyprland;
in
  with lib; {
    options.mringelborn.hyprland = with lib.mringelborn; {
      enable = mkEnableOption "Hyprland";
      hyprlock = mkDisableOption "Hyprlock";
      hypridle = mkDisableOption "Hypridle";
      hyprpaper = mkDisableOption "Hyprpaper";
    };
    imports = [
      ./hyprlandSettings.nix
      ./hypridleSettings.nix
    ];
    config = mkIf cfg.enable {
      wayland.windowManager.hyprland = {
        enable = true;
        systemd.enable = true;
        xwayland.enable = true;
      };

      programs.hyprlock = mkIf cfg.hyprlock {
        enable = true;
        settings = with config.lib.stylix.colors; {
          general.grace = 3;
          label = [
            {
              monitor = "";
              halign = "center";
              valign = "center";
              position = "0, 0";
              text = "$DESC";
              text_align = "center";
              color = "#${base08}";
              font_size = 25;
            }
            {
              monitor = "";
              halign = "center";
              valign = "center";
              position = "0, 80";
              text = "cmd[update:1000] echo \"<span foreground='##ff2222'>$(date)</span>\"";
              text_align = "center";
              color = "#${base08}";
              font_size = 25;
            }
          ];
          input-field = [
            {
              size = "400, 50";
              position = "0, -80";
              monitor = "";
              dots_center = true;
              fade_on_empty = false;
              font_color = "rgb(${base05})";
              inner_color = "rgb(${base00})";
              outer_color = "rgb(${base02})";
              outline_thickness = 5;
              placeholder_text = "<i>$PROMPT</i>";
              fail_text = "<i>$FAIL <b>($ATTEMPTS)</b></i>";
              shadow_passes = 2;
            }
          ];
        };
      };
      services.hypridle.enable = mkIf cfg.hypridle true;
      services.hyprpaper.enable = mkIf cfg.hyprpaper true;
    };
  }
