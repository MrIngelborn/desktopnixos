{pkgs, ...}: let
  lockcommand = "${pkgs.hyprlock}/bin/hyprlock";
in {
  services.hypridle.settings = {
    general = {
      lock_cmd = "pidof ${lockcommand} || ${lockcommand}";
      before_sleep_cmd = "loginctl lock-session";
      after_sleep_cmd = "${pkgs.hyprland}/bin/hyprctl dispatch dpms on";
    };
    listener = [
      {
        timeout = 300;
        on-timeout = "loginctl lock-session";
      }
      {
        timeout = 330;
        on-timeout = "${pkgs.hyprland}/bin/hyprctl dispatch dpms off";
        on-resume = "${pkgs.hyprland}/bin/hyprctl dispatch dpms on";
      }
      {
        timeout = 600;
        on-timeout = "systemctl suspend";
      }
    ];
  };
}
