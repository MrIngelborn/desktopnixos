{pkgs, ...}: {
  wayland.windowManager.hyprland.settings = {
    exec-once = "waybar";
    bindr = [
      "SUPER, SUPER_L, exec, pkill wofi || wofi -I --show drun"
    ];
    bind =
      [
        "SUPER, Q, killactive"
        "SUPER, F, exec, firefox"
        "SUPER CTRL, F, exec, firefox --ProfileManager"
        "SUPER SHIFT, F, exec, firefox --private-window"
        "SUPER, T, exec, alacritty"
        "SUPER, L, exec, ${pkgs.hyprlock}/bin/hyprlock"
        "SUPER SHIFT, L, exit"

        "SUPER, V, togglefloating"
        "SUPER, J, togglesplit"

        "SUPER, up,    movefocus, u"
        "SUPER, down,  movefocus, d"
        "SUPER, left,  movefocus, l"
        "SUPER, right, movefocus, r"
        "SUPER shift, up,    movewindow, u"
        "SUPER shift, down,  movewindow, d"
        "SUPER shift, left,  movewindow, l"
        "SUPER shift, right, movewindow, r"

        ", Print, exec, ${pkgs.hyprshot}/bin/hyprshot -c -m output -o $(xdg-user-dir PICTURES)/Screenshots -f $(date +'%Y-%m-%d-%H%M%S.png')"
        "CTRL, Print, exec, ${pkgs.hyprshot}/bin/hyprshot -m region -o $(xdg-user-dir PICTURES)/Screenshots -f $(date +'%Y-%m-%d-%H%M%S.png')"
        "SHIFT, Print, exec, ${pkgs.hyprshot}/bin/hyprshot -c -m window -o $(xdg-user-dir PICTURES)/Screenshots -f $(date +'%Y-%m-%d-%H%M%S.png')"
        ", XF86AudioMute, exec, ${pkgs.wireplumber}/bin/wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle"
      ]
      ++ (
        # workspaces
        # binds SUPER + [shift +] {1..10} to [move to] workspace {1..10}
        builtins.concatLists (builtins.genList (
            x: let
              ws = let
                c = (x + 1) / 10;
              in
                builtins.toString (x + 1 - (c * 10));
            in [
              "SUPER, ${ws}, workspace, ${toString (x + 1)}"
              "SUPER SHIFT, ${ws}, movetoworkspace, ${toString (x + 1)}"
            ]
          )
          10)
      );
    bindm = [
      "SUPER, mouse:272, movewindow" # Mod + Left mouse button to move
      "SUPER, mouse:274, resizewindow" # Mod + Scroll-click to resize
    ];
    bindl = [
      "SUPER, P, exec, systemctl suspend"
      "SUPER CTRL, P, exec, systemctl reboot"
      "SUPER SHIFT, P, exec, systemctl poweroff"
    ];
    bindel = [
      ", XF86AudioRaiseVolume, exec, ${pkgs.wireplumber}/bin/wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ 5%+"
      ", XF86AudioLowerVolume, exec, ${pkgs.wireplumber}/bin/wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-"
    ];
    monitor = [
      "DP-1, 3840x2160@120, 0x0, 1, vrr, 2, bitdepth, 10"
      "DP-2, 1920x1080@60, -1080x0, 1, transform, 3"
    ];
    input = {
      kb_layout = "se";
      kb_model = "pc104";
      kb_variant = "nodeadkeys";
      numlock_by_default = true;
      follow_mouse = 2;
    };
    general = {
      gaps_in = 3;
      gaps_out = 6;

      resize_on_border = true;
    };
    dwindle = {
      smart_split = false;
      preserve_split = true;
    };
    decoration = {
      rounding = 4;
    };
    plugin = {
      hyprbars = {
        bar_color = "rgba(20508080)";
        bar_height = 20;

        bar_part_of_window = "true";

        bar_text_font = "mono";
        bar_text_size = 12;

        hyprbars-button = [
          "rgb(ff4040), 14, , hyprctl dispatch killactive"
          "rgb(eeee11), 14, , hyprctl dispatch fullscreen 1"
        ];
      };
    };
  };
}
