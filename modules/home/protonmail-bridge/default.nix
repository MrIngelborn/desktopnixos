{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.services.protonmail-bridge;
in {
  options.services.protonmail-bridge = {
    enable = mkEnableOption "Whether to enable the Bridge.";

    logLevel = mkOption {
      type = types.enum ["panic" "fatal" "error" "warn" "info" "debug" "debug-client" "debug-server"];
      default = "info";
      description = "The log level";
    };
  };

  ##### implementation
  config = mkIf cfg.enable {
    home.packages = [pkgs.protonmail-bridge];

    systemd.user.services.protonmail-bridge = {
      Unit = {
        Description = "Protonmail Bridge";
        After = ["network.target"];
      };

      Service = {
        Restart = "always";
        ExecStartPre = "${pkgs.coreutils}/bin/sleep 20";
        ExecStart = "${pkgs.protonmail-bridge}/bin/protonmail-bridge -n --log-level ${cfg.logLevel}";
      };

      Install = {
        WantedBy = ["default.target"];
      };
    };
  };
}
