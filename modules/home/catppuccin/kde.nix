{
  config,
  lib,
  ...
}: let
  cfg = config.mringelborn.catppuccin;
  kdeCfg = config.mringelborn.kde;
  flavor = lib.mringelborn.toUpperFirstChar config.catppuccin.flavor;
  accent = lib.mringelborn.toUpperFirstChar config.catppuccin.accent;
in
  with lib; {
    config = mkIf (cfg.enable && kdeCfg.enable) {
      programs.plasma = {
        workspace = {
          colorScheme = "Catppuccin${flavor}${accent}";
          cursor.theme = "Catppuccin-${flavor}-${accent}-Cursors";
        };
      };
    };
  }
