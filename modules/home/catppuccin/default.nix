{
  config,
  lib,
  ...
}: let
  cfg = config.mringelborn.catppuccin;
in
  with lib; {
    options.mringelborn.catppuccin = {
      enable = mkEnableOption "MrIngelborn Catppuccin";
    };

    config = mkIf cfg.enable {
      catppuccin = {
        enable = true;
        accent = "sky";
      };
      qt = {
        style.name = mkForce "kvantum";
        platformTheme.name = mkForce "kvantum";
      };
    };
  }
