{
  config,
  lib,
  pkgs,
  inputs,
  system,
  ...
}: let
  cfg = config.mringelborn.firefox;
  package = pkgs.wrapFirefox pkgs.firefox-unwrapped {
    nativeMessagingHosts = [(pkgs.callPackage ./firefox-profile-switcher-connector.nix {})];
    extraPolicies = {ExtensionSettings = {};};
  };
  profile = {
    extensions = with inputs.nur.legacyPackages."${system}".repos.rycee.firefox-addons; [
      profile-switcher
      ublock-origin
      bitwarden
      sponsorblock
      dearrow
      darkreader
    ];
    search = {
      default = "Kagi";
      force = true;
      order = ["Kagi" "DuckDuckGo"];
      engines = {
        "Kagi".urls = [
          {
            template = "https://kagi.com/search?";
            params = [
              {
                name = "q";
                value = "{searchTerms}";
              }
            ];
          }
        ];
      };
    };
  };
in
  with lib; {
    options.mringelborn.firefox = {
      enable = mkEnableOption "MrIngelborn Firefox";
      profileNames = mkOption {
        type = types.listOf types.str;
        default = [
          "Private"
          "MoS"
          "Trasjo"
        ];
      };
    };

    config.xdg.configFile = mkIf cfg.enable {
      "firefoxprofileswitcher/config.json".text = ''
        {"browser_binary": "${package}/bin/firefox"}
      '';
    };

    config.programs.firefox = mkIf cfg.enable {
      enable = true;
      inherit package;
      profiles = builtins.listToAttrs (
        lists.imap0 (
          id: name: {
            inherit name;
            value =
              {
                inherit id;
                isDefault = id == 0;
              }
              // profile;
          }
        )
        cfg.profileNames
      );
    };
  }
