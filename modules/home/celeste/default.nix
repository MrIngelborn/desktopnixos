{
  config,
  pkgs,
  lib,
  ...
}:
with lib; let
  cfg = config.services.celeste;
in {
  options.services.celeste = {
    enable = mkEnableOption "Whether to enable the Celeste service.";
    package = mkOption {
      type = types.package;
      default = pkgs.celeste;
      description = "The package of celeste to use";
    };
    remotes = mkOption {
      type = with types;
        attrsOf (listOf (submodule {
          options = {
            localPath = mkOption {
              type = path;
            };
            remotePath = mkOption {
              type = str;
            };
          };
        }));
      default = {};
      description = "";
    };
  };

  config = mkIf cfg.enable {
    home.packages = [cfg.package];

    systemd.user.services.celeste = let
      createRemoteTable = "CREATE TABLE IF NOT EXISTS remotes ( id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT NOT NULL ); DELETE FROM remotes;";
      remoteInserts = strings.concatStringsSep " " (
        lists.imap1 (index: value: "INSERT INTO remotes (id,name) VALUES (${toString index},'${value}');") (
          attrsets.mapAttrsToList (
            name: _value: name
          )
          cfg.remotes
        )
      );
      remoteValues = attrsets.attrValues cfg.remotes;
      remoteValuesWithIndex = lists.imap1 (index: list: lists.forEach list (value: value // {remoteId = toString index;})) remoteValues;
      withId = lists.imap1 (index: value: (value // {id = toString index;})) (lists.flatten remoteValuesWithIndex);
      syncDirs = strings.concatStringsSep " " (
        lists.forEach withId (config: "INSERT INTO sync_dirs (id,remote_id,local_path,remote_path) VALUES (${config.id}, ${config.remoteId}, ${config.localPath}, ${config.remotePath});")
      );
      databaseSetup = "echo ${createRemoteTable} ${remoteInserts} ${syncDirs}";
    in {
      Unit = {
        Description = "Celeste";
        After = ["network.target"];
      };
      Service = {
        Restart = "always";
        ExecPreStart = databaseSetup;
        ExecStart = "${cfg.package}/bin/celeste --background";
      };
      Install.WantedBy = ["default.target"];
    };
  };
}
