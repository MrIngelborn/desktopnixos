{
  inputs,
  lib,
  config,
  pkgs,
  ...
}: let
  cfg = config.mringelborn.base16;

  convertSvgToPng = name: svg: (pkgs.stdenv.mkDerivation {
    name = "${name}.png";
    nativeBuildInputs = [pkgs.imagemagick];
    phases = ["buildPhase" "installPhase"];
    buildPhase = ''
      convert -resize 3840x2160 -depth 8 ${svg} converted.png
    '';
    installPhase = ''
      cp converted.png $out
    '';
  });
  wallpaper-base16-nix = convertSvgToPng "wallpaper-base16-nix" (config.lib.stylix.colors {
    templateRepo = inputs.base16-wallpapers;
    target = "nix-catppuccin";
  });
in
  with lib; {
    imports = [
      ./firefox.nix
    ];

    options.mringelborn.base16 = with lib.mringelborn; {
      enable = mkEnableOption "MrIngelborn Base16";
      scheme = mkOption {
        type = types.str;
      };
      wallpaperTemplate = mkOption {
        type = types.str;
      };
      hyprlock = mkDisableOption "Hyprlock backgrouds";
      hyprpaper = mkDisableOption "Hyprpaper wallpapers";
      monitors = mkOption {
        description = "List of monitors to apply wallpapers to";
        type = with types; listOf str;
      };
    };

    config = mkIf cfg.enable {
      stylix = {
        enable = true;
        #image = mkIf cfg.wallpaperTemplate wallpaper-base16-nix;
        #base16Scheme = mkIf (cfg.scheme != null) cfg.scheme;
        targets.waybar = {
          enableCenterBackColors = true;
          #enableLeftBackColors = true;
          #enableRightBackColors = true;
        };
      };

      programs.hyprlock.settings.background = mkIf cfg.hyprlock [
        {
          path = "${wallpaper-base16-nix}";
        }
      ];
      services.hyprpaper.settings = mkIf cfg.hyprpaper {
        preload = [
          "${wallpaper-base16-nix}"
        ];
        wallpaper = lists.forEach cfg.monitors (monitor: "${monitor},${wallpaper-base16-nix}");
      };
    };
  }
