{lib, ...}: {
  mkDisableOption = name: lib.mkEnableOption name // {default = true;};
  toUpperFirstChar = s: let
    firstChar = lib.toUpper (lib.substring 0 1 s);
    rest = lib.substring 1 (-1) s;
  in
    firstChar + rest;
}
