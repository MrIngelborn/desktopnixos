{
  inputs,
  system,
  ...
}:
inputs.pre-commit-hooks.lib.${system}.run {
  src = ../../.;
  hooks = {
    alejandra.enable = true;
    deadnix = {
      enable = true;
      settings = {
        edit = true;
      };
    };
    statix.enable = true;
  };
}
